const state = {
  funds: 10000,
  stocks: []
};

const mutations = {
  // passing destructured payload
  'BUY_STOCK' (state, {stockId, quantity, stockPrice}) {
    // look if we've already bought that stock type  
    const record = state.stocks.find(el => el.id === stockId);
    if (record) {
      record.quantity += quantity;
    } else {
      state.stocks.push({
        id: stockId,
        quantity: quantity,
        stockPrice: stockPrice
      });
    }
    console.debug('BUY STOCK', state);
    state.funds -= quantity * stockPrice;
  },
  'SELL_STOCK' (state, {stockId, quantity, stockPrice}) {
    // check if the stock exists
    const record = state.stocks.find(el => el.id === stockId)
    let leftQuantity = 0;
    if (record.quantity > quantity) {
      record.quantity -= quantity;
      leftQuantity = quantity;
      if (!record.quantity) {
        state.stocks.splice(state.stocks.indexOf(record), 1);
      }
    } else {
      state.stocks.splice(state.stocks.indexOf(record), 1);
      leftQuantity = record.quantity;
    }
    state.funds += leftQuantity * stockPrice;
  }
}

const actions = {
  sellStock({commit}, order) {
    commit('SELL_STOCK', order);
  }
};

const getters = {
  stockPortfolio (state, getters) {
    return state.stocks.map(stock => {
      const record = getters.stocks.find(el => el.id === stock.id);
      return {
        id: stock.id,
        quantity: stock.quantity,
        name: record.name,
        price: record.price
      }
    });
  },
  funds (state) {
    return state.funds;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
}